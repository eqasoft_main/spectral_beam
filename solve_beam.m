% /*****************************************************************************
%   Copyright (c) 2020, EQASoft IN.
%   All rights reserved.
% 
%   Redistribution and use in source and binary forms, with or without
%   modification, are permitted provided that the following conditions are met:
% 
%     * Redistributions of source code must retain the above copyright notice,
%       this list of conditions and the following disclaimer.
%     * Redistributions in binary form must reproduce the above copyright
%       notice, this list of conditions and the following disclaimer in the
%       documentation and/or other materials provided with the distribution.
%     * Neither the name of EQASoft IN. nor the names of its contributors
%       may be used to endorse or promote products derived from this software
%       without specific prior written permission.
% 
%   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
%   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
%   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
%   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
%   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
%   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
%   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
%   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
%   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
%   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
%   THE POSSIBILITY OF SUCH DAMAGE.
% 
% ******************************************************************************
% * Contents:  This file contains the 'main' function. Program execution begins
%              and ends there.
% 
% * Developed by :
%             Avisek Mukherjee (amukherjee@eqasoft.com)
% 
% * Generated July 2015
% *****************************************************************************/



% solve_beam
clear all;
%close all;
clc;

fn = 20;       % No. of samples in frequency domain

funKd = @sfem_fun_beam2;
fmin = 40;   % Minimum frequency (Hz)
fmax = 50;    % Maximum frequency (Hz)

%% interpolation
% interpolation points on real axis
% f = chebpts(fn,[fmin,fmax]);
% w = [1/2; ones(fn-2,1); 1/2].*(-1).^((0:fn-1)');

% interpolation points on circle
radius = (fmax - fmin)/2;
center = fmin + radius;
z = exp(2i*pi*(0:fn-1)/fn);
f = radius*z + center;

% samples of Kd
Kd = funKd(f);
s = size(Kd{1},1);

%% pole handling
[poles,residues] = polesKd(funKd,s,radius,center);   % compute poles
P = removepoles(Kd,z,poles,residues);                % remove poles
%P=Kd;
%% solve nlep
[C0,C1] = linlagr(P,z,z);   % linearization
[V,D] = eig(C0,C1);         % solve glep

lam = radius*diag(D) + center;
err = zeros(size(lam));
for i = 1:length(lam)
    u = V(1:s,i);
    u = u/norm(u);
    Kdlam = funKd(lam(i));
    err(i) = norm(Kdlam*u);
end

%% output
tol = 1e-6;
%[lam(err<tol),err(err<tol)]

feq = sort(abs(lam(err<tol)))
%% plot
figure
hold off; plot(radius*exp(2i*pi*(0:fn-1)/fn) + center,'-k'); hold on;
plot(lam,'*b');
plot(lam(err<tol),'sg');
plot(radius*poles + center,'or');
title(['fn = ',num2str(fn)]);
